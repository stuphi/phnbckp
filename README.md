# Phnbckp


Phone Backup Utility

This is intended to be a simple utility to enable me to backup the stuff from my phone that is not in the cloud. How useful it will be remains to be seen.

## Desired features

Following are a few notes on the desired features.

1. Work well on Linux and possibly Windows.
2. Backup all user installed apps and associated data.
3. Optionally backup system installed apps data only.
4. Backup SMS messages and phone call list.
5. Backup up Alarms.
6. Remove apps that have been removed in the past.
7. Keep track of the phone so that it only backup what is new/changed.
8. Restore selected app/data only.
9. Allow restoring app/data from one device on a different device.

## Phase One

Initialization will require the following steps.

1. Check that _adb_ is available.
2. Check that a phone is connected, and if so, do we know this phone.
3. List all installed software.
4. List backed up software/data.

### List all installed software.

This list is gathered from the command:

    adb shell dumpsys package

This command will generate a mass of information that we can pick through to find the stuff we need. This can then be stored ins a suitable structure.
