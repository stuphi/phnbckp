#!/usr/bin/python3

# phnbckp.py Copyright (C) 2012, Philip Stubbs
# Could destroy all data on your phone. Do not use!!

ver = 0.1

import subprocess
import re
import os
import errno

def devices():
    """
    Returns a list of all attached devices. Relies on adb being available.
    """
# This is just a place to dump stuff we don't want on screen
    filenull = open(os.devnull, "w")
    devices = []
# Here we kill any running adb server.
    temp = subprocess.check_call("adb kill-server", shell=True)
# New we atart up the server.
    temp = subprocess.check_call("adb start-server", stdout=filenull, stderr=filenull, shell=True)
# Now lets grab the list of running devices.
    data = subprocess.check_output("adb devices", shell=True)
    for d in data.splitlines():
        if re.search(b'\tdevice', d):
            devices.append(re.search(b'[\S]+',d).group())
# And now we return the list.
    return devices

def pckgLst(lst):
    """
    Remove the first eight charecters from each element in the list so that we
    have only the name. Also clean up newlines.
    """
    ret = []
    for e in lst.splitlines():
        ret.append(e[8:])
    return ret

def grabData(device):
    """
     Grab the data from the device. Return a structure that we can work with.
    Structure contains a dict of package names. Each name associates with a
    dict containg codePath, dataDir and system flag.
    """
    ret = {}
# This will grab the raw data.
    data = subprocess.check_output("adb -s " + device + " shell dumpsys package", shell=True)
# Here have a list of the third part packages. 
    apps_3 = pckgLst(subprocess.check_output("adb -s " + device + " shell pm list packages -3", shell=True))
    for d in data.splitlines():
        if re.search(b'^  Package \[', d):
            package = d[11:-13]
            ret[package] = {}
            if package in apps_3: # Assume system app if not 3rd party
                ret[package]['system'] = 0
            else:
                ret[package]['system'] = 1
        if re.search(b'^    codePath=', d):
            ret[package]['codePath'] = d[13:].decode('utf8')
        if re.search(b'^    dataDir=', d):
            ret[package]['dataDir'] = d[12:].decode('utf8')
    return ret

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else: raise

def main():
    print("This is a phone backup utility for my Android phone")
    print("Version %0.1f" % ver)
    print("Finding attached devices...")
# Grab the data
    devs = devices()
# If we have more or less than one device bail out now!
    if len(devs) != 1:
        print("There needs to be one device attached for this to work.")
        exit(0)
    for d in devs:
        d = d.decode('utf8')
        data = grabData(d)
        apk_dir = d + "/apks"
        mkdir_p(apk_dir)
        db_dir = d + "/dbs"
        mkdir_p(db_dir)
#        print(data)
        for e in data:
            if data[e]['system'] == 0:
                print(e.decode('utf8') + " %d" % data[e]['system'])
#                subprocess.check_call("adb -s " + d + " pull " + data[e]['codePath'] + " " \
#                    + apk_dir + "/", shell=True)
        cmd = "adb -s " + d + " pull "
        cmd = cmd + "/data/data/com.android.providers.telephony/databases/"
        cmd = cmd + "mmssms.db " + db_dir + "/mmssms.db"
        print(cmd)
        subprocess.check_call(cmd)
    exit(0)

if __name__ =='__main__':
    main()
